(ns sum-test
  (:require [clojure.test :refer [deftest is]]
            sum))

(deftest one-plus-two-test
  (is (= 3 (sum/sum 1 2)))
)

